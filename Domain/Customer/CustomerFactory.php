<?php

namespace Bookstore\Domain\Customer;

use Bookstore\Domain\Customer;

class CustomerFactory {
  public static function factory(
    string $type,
    int $id,
    string $firstname,
    string $surname,
    string $email
  ): Customer {
    $classname = __NAMESPACE__ . '\\' . ucfirst($type); // TRY "\\" as well
    if (!class_exists($classname)) {
      throw new InvalidArgumentException('Wrong argument.'); // ('Wrong argument.' $e);
    }

    return new $classname($id, $firstname, $surname, $email);
  }
}

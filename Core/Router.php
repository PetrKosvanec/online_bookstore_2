<?php
namespace Bookstore\Core;
use Bookstore\Controllers\ErrorController;
use Bookstore\Controllers\CustomerController;

class Router {
  private $routeMap;
  private static $regexPatters = [
  'number' => '\d+',
  'string' => '\w'
  ];

  public function __construct() {
    $json = file_get_contents(
      __DIR__ . '/../config/routes.json'
    );
    $this->routeMap = json_decode($json, true);
  }

  public function route(Request $request): string {
    $path = $request->getPath();
    // getPath() WORKS GREAT


    foreach ($this->routeMap as $route => $info) {
      // $this->routeMap as $route => $info WORKS GREAT, $route FOR SURE, $info HIGHLY PROBABLY

      $regexRoute = $this->getRegexRoute($route, $info);
      // the above line WORKS GREAT
/*
      echo $route . ": " . $info;
      echo "<br>";
      echo $regexRoute;
      echo "<br><br>";

      echo "<br><br><br>)";
      var_dump($regexRoute);
      echo "<br><br><br>";
*/
      // echo $path . "<br>";
      // echo "@^/$regexRoute$@" . "<br><br>";

      if (preg_match("@^/$regexRoute$@", $path)) {
        echo "if's condition is TRUE<br><br>";
        echo "\$route: " . $route . "<br>";
        echo "\$path: " . $path . "<br><br>";
        echo "\$info's var_dump:<br>";
        var_dump($info);
        echo "<br><br>";
        echo "var_dump(\$request): <br>";
        var_dump($request);
        echo "<br><br>";
        echo $this->executeController($route, $path, $info, $request);
        var_dump($this->executeController($route, $path, $info, $request));
        // echo "if's condition is TRUE";
        // echo "if's condition is TRUE";
        // echo "if's condition is TRUE";
        return $this->executeController(
          $route, $path, $info, $request
        );
      }

    }
    $errorController = new ErrorController($request);
    return $errorController->notFound();
  }

  private function getRegexRoute(
    string $route,
    array $info
  ): string {
    if (isset($info['params'])) {
      foreach ($info['params'] as $name => $type) {
        $route = str_replace(
          ':' . $name, self::$regexPatters[$type], $route
        );
      }
    }
    return $route;
  }

  private function extractParams(
    string $route,
    string $path
  ): array {
    $params = [];

    $pathParts = explode('/', $path);
    $routeParts = explode('/', $route);

    foreach ($routeParts as $key => $routePart) {
      if (strpos($routePart, ':') === 0) {
        $name = substr($routePart, 1);
        $params[$name] = $pathParts[$key+1];
      }
    }
    return $params;
  }

  private function executeController(
    string $route,
    string $path,
    array $info,
    Request $request
    ): string {
    $controllerName = '\Bookstore\Controllers\\' . $info['controller'] . 'Controller';
    $controller = new $controllerName($request);

    echo "<br><br><br><br>\$info['login']: " . $info['login'] . "<br><br>var_dump(\$info['login'])";
    var_dump($info['login']);

    if (isset($info['login']) && $info['login']) {
      if ($request->getCookies()->has('user')){
        $customerId = $request->getCookies()->get('user');
        $controller->setCustomerId($customerId);
      }
      else {
        $errorController = new CustomerController($request);
        return $errorController->login('');    // ->login('enter@prise');
      }
    }

    $params = $this->extractParams($route, $path);
    return call_user_func_array(
      [$controller, $info['method']], $params
    );
  }
}

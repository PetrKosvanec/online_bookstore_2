<?php

namespace Bookstore\Core;

class FilteredMap {
    private $map;

    public function __construct(array $baseMap) {
      $this->map = $baseMap;
    }

    public function has(string $name): bool {
      return isset($this->map[$name]);
    }

    public function get(string $name) {
      return $this->map[$name] ?? null;
    }
    /*
    ?? is null coalescing operator

    The expression (expr1) ?? (expr2) evaluates to expr2 if expr1 is NULL, and expr1 otherwise.

    In particular, this operator does not emit a notice if the left-hand side value
       does not exist, just like isset(). This is especially useful on array keys.
    */
    public function getInt(string $name): int {
      return (int) $this->get($name);
    }

    public function getNumber(string $name): float {
      return (float) $this->get($name);
    }

    public function getString(string $name): string {
      $value = (string) $this->get($name);
      return $filter ? addslashes($vallue) : $value;
    }
}

<?php

namespace Bookstore\tests\Domain\Customer;

use Bookstore\Domain\Customer\Basic;
use PHPUnit_Framework_TestCase;

class BasicTest extends PHPUnit_Framework_TestCase {

  public function setUp() {
    $this->customer = new Basic(
      1, 'han', 'solo', 'han@solo.com'
    );
  }

  /**
  * @test
  */
  public function testAmountToBorrow() {

    $this->assertSame(
      3,
      $this->customer->getAmountToBorrow(),
      'Basic customer should borrow up to 3 books.'
    );
  }

  /**
  * @test
  */
  public function testIsExemptOfTaxes() {
    $this->assertFalse(
    $this->customer->isExemptOfTaxes(),
    'Basic customer should not be exempt of taxes.'
    );
  }

  public function testGetMonthlyFee() {
    $this->assertEquals(
      5,
      $this->customer->getMonthlyFee(),
      'Basic customer should pay 5 a month.'
      );
    }

}
